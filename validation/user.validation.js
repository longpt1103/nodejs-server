module.exports.createPost = function(req,res,next){
	var errors = [];
	if(!req.body.name){
		errors.push('Require Name');
	}
	if(!req.body.phone){

		errors.push('Require Phone');
	}
	if(errors.length){
		res.render('users/create',{
			errors:errors,
			values:req.body
		});
		return;
	}
	next();
}

var express = require('express');
var app = express();
var router = express.Router();
var controller = require('../controller/prod.controller');
//var validation = require('../validation/auth.validation');


router.get('/',controller.product);
router.post('/',controller.creatPost);
router.patch('/:id',controller.productPatch);
router.delete('/:id',controller.productDelete);

module.exports = router;
var User = require('../models/user.model');
var md5 = require('md5');
module.exports.index = async function(req,res){
	var users = await User.find();
	res.render('users/index',{
		users: users
	});
}
module.exports.create = function(req,res){
	res.render('users/create');
}
module.exports.createPost = async function(req,res){
	var hashedPassword = req.body.password;
	req.body.password = md5(hashedPassword);
	req.body.avatar = req.file.path.split('/').slice(1).join('/'); 
	await User.create(req.body);
	res.redirect('/users');
}
module.exports.search = async function(req,res){
	var q = req.query.q.toLowerCase();
	var users = await User.find();
	var usersMatched = await users.filter(function(user) {
		return user.name.toLowerCase().indexOf(q) >= 0;
	});
	res.render('users/index', {
		users : usersMatched
	});
}
module.exports.get = async function (req, res) {
	var id = req.params.id
	var user = await User.findById(id);
	res.render('users/view', {
		user : user
	});
}
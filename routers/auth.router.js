
var express = require('express');
var app = express();
var router = express.Router();
var controller = require('../controller/auth.controller');
//var validation = require('../validation/auth.validation');


router.get('/login',controller.login);
router.post('/loginPost',controller.loginPost);

module.exports = router;
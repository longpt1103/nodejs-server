var db = require('../db');
var md5 = require('md5');
module.exports.login = function(req,res){
	res.render('auth/login');
}
module.exports.loginPost = function(req,res){
	var username = req.body.username;
	var password = req.body.password;
	var hashedPassword = md5(password);
	var user = db.get('users').find({ name:username }).value() ;
	if(!user){
		res.render('auth/login',{
			errors : [
				'User does not exits'
			],
			values:res.body
		});
		return;
	}
	if(user.password !== hashedPassword ){
		res.render('auth/login',{
			errors : [
				'Wrong password'
			],
			values:res.body	
		});
		return;
	}
	res.cookie('userId', user.id,{
		signed:true
	});
	res.redirect('/users');
}
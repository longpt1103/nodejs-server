
var express = require('express');
var app = express();
var multer  = require('multer');
var router = express.Router();
var controller = require('../controller/user.controller');
var validation = require('../validation/user.validation');



var upload = multer({ dest: 'public/uploads/' });

router.get('/',controller.index);

router.get('/create',controller.create);

router.post('/create',upload.single('avatar'),validation.createPost,controller.createPost);

router.get('/search',controller.search);

router.get('/:id', controller.get);

module.exports = router;
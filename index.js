require('dotenv').config();
var express = require('express');
var app = express();
var mongoose = require('mongoose');
mongoose.connect(process.env.MONGO_URL,{useNewUrlParser: true});

var routerUser = require('./routers/user.router');
var routerAuth = require('./routers/auth.router');
var routerProd = require('./routers/prod.router');
var routerCart = require('./routers/cart.router');

var apiRouterProd = require('./api/routers/prod.router');

var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');

var authMiddleware = require('./middlewares/auth.middleware');
var sessionMiddleware = require('./middlewares/session.middleware');

var port = 3000;
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.set('view engine', 'pug');
app.set('views', './views');

app.use(express.static('public'));
app.use(cookieParser(process.env.SESSION_SECRET));
app.use(sessionMiddleware);




app.use('/auth', routerAuth);
app.use('/products', routerProd);
app.use('/users', routerUser);
app.use('/cart',routerCart);
app.use('/api/products',apiRouterProd);




app.listen(port,function(){
	console.log('serve listen on port'+ port);
});
var Product = require('../../models/product.model');


module.exports.product = async function(req,res){
	products = await Product.find();
	res.json(products);
}
module.exports.creatPost = async function(req,res){
	var data = req.body;
	var product = await Product.create(data);
	res.json(product);
}
module.exports.productPatch = async function(req,res){
	var id = req.params.id;
	var updatedData = req.body; 
	var product = await Product.findById(id);
	if(product.id != null){
		// update data
		var productUpdate = await Product.findOneAndUpdate(id,updatedData);
		// return
		res.json(productUpdate);
	}else{
		res.end("Don't Exist Customer" );
	}
}
module.exports.productDelete = async function(req,res){
	var id = req.params.id;
	var updatedData = req.body; 
	var product = await Product.findById(id);
	if(product.id != null){
		// update data
		var productDelte = await Product.findOneAndDelete(id,updatedData);
		// return
		res.json(productDelte);
	}else{
		res.end("Don't Exist Customer" );
	}
}